package com.example.shiam.radionurani;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TabLayout mTabLayout;
    ViewPager mViewPager;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    final String PRAYER_URL = "http://api.aladhan.com/v1/calendar";
    final String SCHOOL = "1";
    final String METHOD = "1";
    final String MONTH = "08";
    final String YEAR = "2018";
    PrayerDataModel prayerModel;
    public JSONArray tempJsonArray = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mViewPager = (ViewPager) findViewById(R.id.viewPager_id);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new CurrentFragment(), "");
        adapter.addFragment(new PrayerFragment(), "");
        adapter.addFragment(new CompassFragment(), "");
        adapter.addFragment(new ScheduleFragment(), "");


        mViewPager.setAdapter(adapter);
        mTabLayout = (TabLayout) findViewById(R.id.tablayout_id);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(0).setIcon(R.drawable.current2);
        mTabLayout.getTabAt(1).setIcon(R.drawable.prayer2);
        mTabLayout.getTabAt(2).setIcon(R.drawable.compass2);
        mTabLayout.getTabAt(3).setIcon(R.drawable.schedule2);

        prayerModel = new PrayerDataModel();

        Log.d("main", "on create main called");

        //getCurrentLocation();
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("main", "on resume called in main");
        /*SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        String [] dateParts = thisDate.split("/");
        String day = dateParts[0];
        String month = dateParts[1];
        String year = dateParts[2];

        Log.d("time", thisDate);
        Log.d("time", "day : " + day);
        Log.d("time", "month : " + month);
        Log.d("time", "year : " + year); */
        getCurrentLocation();
    }

    public void getCurrentLocation(){
        Log.d("main", "getcurrentlocation method called");
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("Location: ", location.toString());
                String longitude = String.valueOf(location.getLongitude());
                String latitude = String.valueOf(location.getLatitude());


                Log.d("Location", "longitude is: " + longitude);
                Log.d("Location", "latitude is: " + latitude);

                RequestParams params = new RequestParams();
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("school", SCHOOL);
                params.put("method", METHOD);
                params.put("month", MONTH);
                params.put("year", YEAR);

                letsDoSomeNetworking(params);

                //to pass data to another fragment
                /*Bundle bundle = new Bundle();
                bundle.putString("lat", latitude);
                bundle.putString("lon", longitude);

                //set Fragment Class Argument
                PrayerFragment mPrayerFragment = new PrayerFragment();
                mPrayerFragment.setArguments(bundle); */
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION},1);
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 1000  , mLocationListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 1){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Log.d("Location", "permission granted");
                /*if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED){
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
                }*/
                getCurrentLocation();
            }else {
                Log.d("Location : " , "Permission denied!!");
            }
        }


    }

    public void updatePrayerUI(){
        SimpleDateFormat currentDate = new SimpleDateFormat("dd/MM/yyyy");
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        String [] dateParts = thisDate.split("/");
        String day = dateParts[0];
        Map<String, String> recentData = prayerModel.getUIData(day);
        Log.d("update", recentData.get("fazar"));
        if(recentData != null){
            TextView fazar_time = (TextView) findViewById(R.id.fazar_time);
            TextView zohor_time = (TextView) findViewById(R.id.zohor_time);
            TextView asar_time = (TextView) findViewById(R.id.asar_time);
            TextView magrib_time = (TextView) findViewById(R.id.magrib_time);
            TextView esha_time = (TextView) findViewById(R.id.esha_time);

            if(fazar_time != null){
                fazar_time.setText(recentData.get("fazar"));
            }
            if(zohor_time != null){
                zohor_time.setText(recentData.get("zohor"));
            }
            if(asar_time != null){
                asar_time.setText(recentData.get("asar"));
            }
            if(magrib_time != null){
                magrib_time.setText(recentData.get("magrib"));
            }
            if(esha_time != null){
                esha_time.setText(recentData.get("esha"));
            }
        }


        //Date c = Calendar.getInstance().getTime();

    }

    public void play(View view){
        ImageView play = (ImageView) findViewById(R.id.play);
        ImageView pause = (ImageView) findViewById(R.id.pause);

        int flag = Math.round(play.getAlpha());

        if(flag == 1){
            play.animate().alpha(0f).setDuration(500);
            pause.animate().alpha(1f).setDuration(500);
        }else{
            play.animate().alpha(1f).setDuration(500);
            pause.animate().alpha(0f).setDuration(500);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.action_location){
            //Toast.makeText(this, "location selected", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // TODO: Add letsDoSomeNetworking(RequestParams params) here:
    private  void letsDoSomeNetworking(RequestParams params){
        AsyncHttpClient client = new AsyncHttpClient();
        Log.d("Clima", "lets do some net called");
        client.get(PRAYER_URL, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //super.onSuccess(statusCode, headers, response);
                /*Log.d("Clima", "Success! Json: " + response.toString());*/
                //Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_SHORT ).show();
                try {
                    JSONArray data = response.getJSONArray("data");
                    if(tempJsonArray != data){
                        if(prayerModel.checkListSize() > 0){
                            prayerModel.clearList();
                        }
                        for(int i = 0 ; i < data.length() ; ++i){
                            JSONObject obj = data.getJSONObject(i);
                            JSONObject timing = obj.getJSONObject("timings");
                            prayerModel.fromJson(timing);
                            //Log.d("Hello", timing.toString());
                        }
                        tempJsonArray = data;
                        updatePrayerUI();
                    }

                    //convertJsonToArray(data);
                    //Log.d("Hello", data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //PrayerDataModel prayerData = PrayerDataModel.fromJson(response);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject response) {
                //super.onFailure(statusCode, headers, e, response);
                Log.d("Clima", "Fail " + e.toString());
                Log.d("Clima", "Status code " + statusCode);
                Toast.makeText(MainActivity.this, "Request Failed!", Toast.LENGTH_SHORT ).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("main", "on pause called in main");
    }
}
