package com.example.shiam.radionurani;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CurrentFragment extends Fragment{

    View view;
    public CurrentFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("current", "on attach called");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("current", "on create called");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_current, container, false);
        Log.d("current", "on create view called");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("current", "on activity created called");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("current", "on start called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("current", "on resume called");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("current", "on save instant state called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("current", "on pause called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("current", "on stop called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("current", "on destroy view called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("current", "on distroy called");
    }
}
