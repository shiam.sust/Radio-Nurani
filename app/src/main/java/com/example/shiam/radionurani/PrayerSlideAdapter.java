package com.example.shiam.radionurani;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class PrayerSlideAdapter extends RecyclerView.Adapter<PrayerSlideAdapter.ViewHolder>{


    public PrayerSlideAdapter() {

    }
    public int[] images = {
            R.drawable.zohor ,
            R.drawable.fazar,
            R.drawable.asar
    };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.prayer_slide, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(images[position])
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return images.length;
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}

/*
private Context mContext;
    LayoutInflater inflater;

    public int[] images = {
            R.drawable.zohor ,
            R.drawable.fazar ,
            R.drawable.asar
    };

    public PrayerSlideAdapter(Context context) {

        mContext = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }



    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view==(RelativeLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.prayer_slide,container,false);
        ImageView img = (ImageView) view.findViewById(R.id.weather);
        img.setImageResource(position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((RelativeLayout)object);

    }
*/
