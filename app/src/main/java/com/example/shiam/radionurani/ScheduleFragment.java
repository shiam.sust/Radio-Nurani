package com.example.shiam.radionurani;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScheduleFragment extends Fragment{

    View view;
    LinearLayout mLinearLayout;

    public ScheduleFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("schedule", "on attach called");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("schedule", "on create called");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_schedule, container, false);
        mLinearLayout = (LinearLayout) view.findViewById(R.id.schedule_view);
        Log.d("schedule", "on create view called");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("schedule", "on activity created called");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("schedule", "on start called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("schedule", "on resume called");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("schedule", "on save instant state called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("schedule", "on pause called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("schedule", "on stop called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("schedule", "on destroy view called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("schedule", "on distroy called");
    }
}
