package com.example.shiam.radionurani;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

public class PrayerFragment extends Fragment{
    View view;
    String strLat = null;
    String strLon = null;

    public PrayerFragment(){

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("prayer", "on attach called");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("prayer", "on create called");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.item_prayer, container, false);
        Log.d("prayer", "on create view called");


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("prayer", "on activity created called");

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("prayer", "on start called");
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle arguments = getArguments();
        if(arguments != null){
            strLat = getArguments().getString("lat");
            strLon= getArguments().getString("lon");
        }
        Log.d("Location", "Lat in fragment : " + strLat);
        Log.d("Location", "Lon in fragment : " + strLon);

        Log.d("prayer", "on resume called");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("prayer", "on save instant state called");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("prayer", "on create called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("prayer", "on create called");
    }

}
