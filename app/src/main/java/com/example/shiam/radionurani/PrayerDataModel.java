package com.example.shiam.radionurani;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrayerDataModel {


    List<Map<String, String>> list = new ArrayList<>();

    public void fromJson(JSONObject jsonObject){

        Map<String, String> row = new HashMap<>();
        try{
            row.put("fazar", jsonObject.getString("Fajr"));
            row.put("zohor", jsonObject.getString("Dhuhr"));
            row.put("asar", jsonObject.getString("Asr"));
            row.put("magrib", jsonObject.getString("Maghrib"));
            row.put("esha", jsonObject.getString("Isha"));
            list.add(row);
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public int checkListSize(){
        return list.size();
    }

    public void clearList(){
        list.clear();
    }

    public Map<String, String> getUIData(String day){
        int foo = Integer.parseInt(day);
        if(list.get(foo - 1) != null){
            return  list.get(foo - 1);
        }else{
            return null;
        }
    }

}
